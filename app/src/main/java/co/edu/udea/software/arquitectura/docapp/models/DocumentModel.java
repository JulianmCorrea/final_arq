package co.edu.udea.software.arquitectura.docapp.models;

public class DocumentModel {
    private String title;
    private int preview;
    private String url;

    public DocumentModel(String title, int preview, String url) {
        this.title = title;
        this.preview = preview;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPreview() {
        return preview;
    }

    public void setPreview(int preview) {
        this.preview = preview;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
