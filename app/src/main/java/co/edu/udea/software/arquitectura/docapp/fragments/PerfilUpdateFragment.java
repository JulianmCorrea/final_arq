package co.edu.udea.software.arquitectura.docapp.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;

import co.edu.udea.software.arquitectura.docapp.R;
import co.edu.udea.software.arquitectura.docapp.dialogs.PasswordDialog;
import co.edu.udea.software.arquitectura.docapp.models.Usuario;
import se.simbio.encryption.Encryption;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class PerfilUpdateFragment extends Fragment implements View.OnClickListener, View.OnLongClickListener {

    //Firebase
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mRef = database.getReference("Users");
    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseUser user = auth.getCurrentUser();


    //View
    ImageView foto;
    Button actualizar, cancelar;
    TextInputEditText email, password, name, date;
    TextView passwordupdate;

    //otros
    Boolean actActivo;
    private Uri uri;
    final private static int DOCUMENT_SELECTED = 3;
    ProgressDialog progressDialog;
    String oldName = "";

    public PerfilUpdateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_perfil_update, container, false);
        email = view.findViewById(R.id.perfilemail);
        password = view.findViewById(R.id.perfilpassword);
        name = view.findViewById(R.id.perfilname);
        date = view.findViewById(R.id.perfilage);
        actualizar = view.findViewById(R.id.perfilupdate);
        cancelar = view.findViewById(R.id.perfilcancel);
        passwordupdate = view.findViewById(R.id.perfilupdatepassword);

        cancelar.setKeyListener(null);
        foto = view.findViewById(R.id.perfilfoto);
        foto.setEnabled(false);
        actualizar.setOnClickListener(this);
        cancelar.setOnClickListener(this);
        foto.setOnClickListener(this);
        passwordupdate.setOnLongClickListener(this);
        actActivo = false;
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("subiendo....");
        progressDialog.setMessage("Actualizando por favor espere");
        progressDialog.setCancelable(false);
        loadData();
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.perfilupdate:
                if (actActivo) {
                    campos();
                    progressDialog.show();
                    subirImagen(user.getUid());
                    actActivo = !actActivo;
                } else {
                    campos();
                    actActivo = !actActivo;
                }
                break;
            case R.id.perfilcancel:
                Glide.with(foto.getContext())
                        .load(user.getPhotoUrl())
                        .into(foto);
                campos();
                actActivo = !actActivo;
                break;

            case R.id.perfilfoto:
                if (actActivo) {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("image/jpeg");
                    intent.addCategory(Intent.CATEGORY_OPENABLE);

                    startActivityForResult(intent, DOCUMENT_SELECTED);
                } else {
                    Toast.makeText(getContext(), "tock", Toast.LENGTH_SHORT).show();
                }
                break;


        }
    }

    @Override
    public boolean onLongClick(View v) {
        if (v.getId() == R.id.perfilupdatepassword) {
            new PasswordDialog(getContext()).show();
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DOCUMENT_SELECTED) {
            if (resultCode == RESULT_OK) {

                uri = data.getData();

                Glide.with(foto.getContext())
                        .load(uri)
                        .into(foto);


            }
        }
    }

    private void campos() {
        if (!actActivo) {
            name.setEnabled(true);
            foto.setEnabled(true);
            cancelar.setVisibility(View.VISIBLE);
        } else {
            name.setEnabled(false);
            foto.setEnabled(false);
            cancelar.setVisibility(View.INVISIBLE);
        }
    }

    private void loadData() {
        mRef.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                Glide.with(foto.getContext())
                        .load(usuario.getPhoto())
                        .into(foto);
                Encryption encryption = Encryption.getDefault("Key", "Salt", new byte[16]);
                String pass = encryption.decryptOrNull(usuario.getPassword());
                password.setText(pass);
                name.setText(usuario.getName());
                email.setText(usuario.getEmail());
                date.setText(usuario.getAge());
                oldName = usuario.getName();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @SuppressWarnings("VisibleForTests")
    private void subirImagen(final String child) {
        if (isNetDisponible()) {
            if (uri != null) {
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRef = storage.getReference("Photos");
                final StorageReference upImg = storageRef.child(child + ".jpg");
                UploadTask uploadTask = upImg.putFile(uri);
                Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        // Continue with the task to get the download URL
                        return upImg.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {

                            final HashMap<String, Object> result = new HashMap<>();
                            UserProfileChangeRequest profile;
                            if (!oldName.isEmpty() && !oldName.equals(name.getText().toString()) && !name.getText().toString().isEmpty()) {
                                result.put("photo", task.getResult().toString());
                                result.put("name", name.getText().toString());
                                profile = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(name.getText().toString())
                                        .setPhotoUri(task.getResult()).build();
                            } else {
                                name.setText(oldName);
                                result.put("photo", task.getResult().toString());
                                profile = new UserProfileChangeRequest.Builder()
                                        .setPhotoUri(task.getResult()).build();

                            }
                            user.updateProfile(profile).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    mRef.child(user.getUid()).updateChildren(result).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            mRef.child(user.getUid()).updateChildren(result).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    uri = null;
                                                    progressDialog.dismiss();
                                                    Toast.makeText(getActivity(), R.string.Okupdate, Toast.LENGTH_LONG).show();
                                                }
                                            });

                                        }
                                    });
                                }
                            });


                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

            } else {
                if (!oldName.isEmpty() && !oldName.equals(name.getText().toString()) && !name.getText().toString().isEmpty()) {
                    final HashMap<String, Object> result = new HashMap<>();
                    result.put("name", name.getText().toString());
                    mRef.child(user.getUid()).updateChildren(result);
                    UserProfileChangeRequest profile = new UserProfileChangeRequest.Builder()
                            .setDisplayName(name.getText().toString()).build();
                    user.updateProfile(profile).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            mRef.child(user.getUid()).updateChildren(result).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    uri = null;
                                    progressDialog.dismiss();
                                    Toast.makeText(getActivity(), R.string.Okupdate, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "Nombre vacio", Toast.LENGTH_LONG).show();

                    name.setText(oldName);
                    progressDialog.dismiss();
                }


            }


        } else {
            Toast.makeText(getActivity(), "Sin conexión a internet", Toast.LENGTH_LONG).show();

            progressDialog.dismiss();
        }
    }

    private boolean isNetDisponible() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
    }


}
