package co.edu.udea.software.arquitectura.docapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import co.edu.udea.software.arquitectura.docapp.R;
import co.edu.udea.software.arquitectura.docapp.dialogs.SignUpDialog;
import co.edu.udea.software.arquitectura.docapp.dialogs.UnlockDialog;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button login, signup;
    private FirebaseAuth mAuth;
    private TextInputEditText user, pass;
    private TextView unlock;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        login = findViewById(R.id.login);
        signup = findViewById(R.id.signup);
        user = findViewById(R.id.user);
        pass = findViewById(R.id.password);
        unlock = findViewById(R.id.unlock);


        login.setOnClickListener(this);
        signup.setOnClickListener(this);
        unlock.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("subiendo....");
        progressDialog.setMessage("subiendo archivo por favor espere");
        progressDialog.setCancelable(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                login();
                break;
            case R.id.signup:
                signup();
                break;
            case R.id.unlock:
                new UnlockDialog(LoginActivity.this).show();

                break;
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser mUser;
        mUser = mAuth.getCurrentUser();
        if (mUser != null) {
            Intent main = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(main);
        }
    }

    private void login() {
        progressDialog.show();
        String email = user.getText().toString();
        String password = pass.getText().toString();
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Intent main = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(main);
                    progressDialog.dismiss();
                }


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });

    }

    private void signup() {
        new SignUpDialog(LoginActivity.this).show();
    }
}
