package co.edu.udea.software.arquitectura.docapp.models;

import java.io.Serializable;

public class Usuario implements Serializable {
    private String uid;
    private String email;
    private String name;
    private String age;
    private String photo;
    private String password;

    public Usuario() {
    }

    public Usuario(String email, String name, String age, String photo, String password) {
        this.email = email;
        this.name = name;
        this.age = age;
        this.photo = photo;
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
