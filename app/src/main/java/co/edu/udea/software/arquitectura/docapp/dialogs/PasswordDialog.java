package co.edu.udea.software.arquitectura.docapp.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;

import co.edu.udea.software.arquitectura.docapp.R;
import co.edu.udea.software.arquitectura.docapp.models.Usuario;
import se.simbio.encryption.Encryption;


public class PasswordDialog extends Dialog implements View.OnClickListener {
    private Button cancelar, cambio;
    TextInputEditText oldPassword, newPassword;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mRef = database.getReference("Users");
    FirebaseAuth auth = FirebaseAuth.getInstance();
    FirebaseUser user;

    private Context cont;
    private Encryption encryption;
    private ProgressDialog progressDialog;

    public PasswordDialog(@NonNull Context context) {
        super(context);
        cont = context;
        Bundle bundle = new Bundle();
        setContentView(R.layout.dialog_password);
        user = auth.getCurrentUser();
        oldPassword = findViewById(R.id.oldPassword);
        newPassword = findViewById(R.id.newPassword);
        cancelar = findViewById(R.id.passwordCancelar);
        cambio = findViewById(R.id.passwordCambio);
        cancelar.setOnClickListener(this);
        cambio.setOnClickListener(this);
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("subiendo....");
        progressDialog.setMessage("Por favor espere");
        progressDialog.setCancelable(false);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.passwordCambio:
                if (noVacios()) {
                    cambiarContraseña();

                }
                break;
            case R.id.passwordCancelar:
                dismiss();
                break;
        }
    }

    private void cambiarContraseña() {
        progressDialog.show();

//Desencripta la contraseña de la base de datos
        encryption = Encryption.getDefault("Key", "Salt", new byte[16]);
        //llamada a la base de datos con actualización automatica en caso de cambio
        mRef.child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Usuario usuario = dataSnapshot.getValue(Usuario.class);
                String password = encryption.decryptOrNull(usuario.getPassword());
                //verifica que la clave actual consida
                if (password.equals(oldPassword.getText().toString())) {
                    //actualiza la contraseña
                    user.updatePassword(newPassword.getText().toString())
                            //Si el caso funciona
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        HashMap<String, Object> map = new HashMap<>();
                                        //cifra la contraseña
                                        String p = encryption.encryptOrNull(newPassword.getText().toString());
                                        map.put("password", p);
                                        mRef.child(user.getUid()).updateChildren(map)
                                                //si se completo la actualización con la clave cifrada nueva
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        Toast.makeText(cont, "Contraseña actualizada", Toast.LENGTH_SHORT).show();
                                                        progressDialog.dismiss();
                                                        dismiss();
                                                    }
                                                    //si no fue capaz de modificar en la base de datos con la contraseña cifrada nueva
                                                }).addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                progressDialog.dismiss();
                                                Toast.makeText(cont, e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }
                                //Si no actualiza
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(cont, e.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(cont, "La contraseña ingresada no coinside con la msotrada en el perfil", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(cont, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean noVacios() {
        boolean ret = false;
        if (!oldPassword.getText().toString().isEmpty() || oldPassword.getText().toString() != null) {
            if (!newPassword.getText().toString().isEmpty() || newPassword.getText().toString() != null) {
                ret = true;
            } else {
                Toast.makeText(cont, "No se ingreso una nueva contraseña", Toast.LENGTH_LONG).show();

            }
        } else {
            Toast.makeText(cont, "No se ingreso la contraseña actual", Toast.LENGTH_LONG).show();

        }
        return ret;
    }
}
