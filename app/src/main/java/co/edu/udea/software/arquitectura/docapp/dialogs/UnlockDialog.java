package co.edu.udea.software.arquitectura.docapp.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import co.edu.udea.software.arquitectura.docapp.R;

public class UnlockDialog extends Dialog implements View.OnClickListener {
    private EditText email;
    private Button ok, cancelar;
    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private ProgressDialog progressDialog;

    public UnlockDialog(@NonNull Context context) {
        super(context);
        Bundle bundle = new Bundle();
        setContentView(R.layout.dialog_unlock);
        email = findViewById(R.id.unlockemail);
        ok = findViewById(R.id.unlockRec);
        cancelar = findViewById(R.id.unlockCan);
        ok.setOnClickListener(this);
        cancelar.setOnClickListener(this);
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("subiendo....");
        progressDialog.setMessage("Por favor espere");
        progressDialog.setCancelable(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.unlockRec:
                restore();
                break;
            case R.id.unlockCan:
                dismiss();
                break;
        }
    }

    private void restore() {
        progressDialog.show();
        String emailAddress = email.getText().toString();
        auth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getContext(), "Se envio un correo de recuperacion", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                            dismiss();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressDialog.dismiss();
                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
