package co.edu.udea.software.arquitectura.docapp.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.util.Calendar;

import co.edu.udea.software.arquitectura.docapp.R;
import co.edu.udea.software.arquitectura.docapp.activities.LoginActivity;
import co.edu.udea.software.arquitectura.docapp.activities.MainActivity;
import co.edu.udea.software.arquitectura.docapp.models.Usuario;
import se.simbio.encryption.Encryption;

public class SignUpDialog extends Dialog implements View.OnClickListener {
    private TextInputEditText email;
    private TextInputEditText password;
    private TextInputEditText name;
    private EditText age;
    private ImageView image;

    private Button confirmar;
    private Button cancelar;
    private Context cont;
    private ProgressDialog progressDialog;


    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la fecha
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);

    //firebase autenticacion
    private FirebaseAuth mAuth;
    //Firebase Database
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mRef = database.getReference("Users");

    public SignUpDialog(Context context) {
        super(context);
        cont = context;
        Bundle bundle = new Bundle();
        setContentView(R.layout.dialog_signup);
        mAuth = FirebaseAuth.getInstance();
        email = findViewById(R.id.signemail);
        password = findViewById(R.id.sigpassword);
        name = findViewById(R.id.signname);
        age = findViewById(R.id.signage);
        image = findViewById(R.id.signage2);

        confirmar = findViewById(R.id.signconfirmar);
        cancelar = findViewById(R.id.signcancel);
        confirmar.setOnClickListener(this);

        cancelar.setOnClickListener(this);
        image.setOnClickListener(this);


        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("subiendo....");
        progressDialog.setMessage("Por favor espere mientras se realiza el registro");
        progressDialog.setCancelable(false);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signconfirmar:
                registrar();
                break;
            case R.id.signcancel:
                dismiss();
                break;
            case R.id.signage2:
                obtenerFecha();
                break;
        }

    }

    private void registrar() {
        if (noVacio(cont)) {
            progressDialog.show();

            final String uEmail = email.getText().toString();
            final String uPassword = password.getText().toString();
            //se realiza una llamada a la api , en rest esto equivale a un post para correo y contraseña
            mAuth.createUserWithEmailAndPassword(uEmail, uPassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        //si se completo la llamada , se realiza una actualización de datos para mandar el nombre para mostrar
                        final FirebaseUser user = mAuth.getCurrentUser();
                        final String url = "https://firebasestorage.googleapis.com/v0/b/final-arqsoft.appspot.com/o/Photos%2FDEFAULT.png?alt=media&token=575fa9a0-4b9f-41a8-8f33-712491167086";
                        Uri uri = Uri.parse(url);
                        UserProfileChangeRequest profile = new UserProfileChangeRequest
                                .Builder().setDisplayName(name.getText().toString()).setPhotoUri(uri)
                                .build();
                        //funcion equivalente a un Update para el usuario creado
                        user.updateProfile(profile).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                //despues de actualizar el usuario guarda un registro del usuario en la BD
                                Encryption encryption = Encryption.getDefault("Key", "Salt", new byte[16]);
                                String encrypted = encryption.encryptOrNull(uPassword);

                                Usuario usuario = new Usuario(uEmail, name.getText().toString(), age.getText().toString(), url, encrypted);
                                mRef.child(user.getUid()).setValue(usuario)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    //confirma que guardo el usuario en la BD inicia la actividad principal
                                                    Toast.makeText(cont, "Registro exitoso", Toast.LENGTH_LONG).show();
                                                    progressDialog.dismiss();
                                                    dismiss();
                                                    Intent main = new Intent(cont, MainActivity.class);
                                                    cont.startActivity(main);
                                                }
                                            }
                                        });
                            }
                        }).addOnFailureListener(new OnFailureListener() { //en caso de fallar el update nos notifica
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                progressDialog.dismiss();
                                Toast.makeText(cont, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }).addOnFailureListener(new OnFailureListener() { //en caso de fallar el post del usuario (registro) nos notifica
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(cont, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    //verifica si los campos no estan vacios
    private boolean noVacio(Context context) {
        boolean res = false;
        if (!email.getText().toString().isEmpty()) {
            if (!password.getText().toString().isEmpty()) {
                if (!name.getText().toString().isEmpty()) {
                    if (!age.getText().toString().isEmpty()) {
                        res = true;
                    } else {
                        Toast.makeText(context, "Debe agregar la fecha de nacimiento", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Debe agregar su nombre", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(context, "Debe agregar una contraseña", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Debe agregar un email", Toast.LENGTH_SHORT).show();
        }
        return res;
    }


    private void obtenerFecha() {
        DatePickerDialog recogerFecha = new DatePickerDialog(cont, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                //Formateo el día obtenido: antepone el 0 si son menores de 10
                String diaFormateado = (dayOfMonth < 10) ? "0" + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10) ? "0" + String.valueOf(mesActual) : String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                age.setText(diaFormateado + "/" + mesFormateado + "/" + year);


            }
            //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
            /**
             *También puede cargar los valores que usted desee
             */
        }, anio, mes, dia);
        //Muestro el widget
        recogerFecha.show();

    }

}
