package co.edu.udea.software.arquitectura.docapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.storage.FirebaseStorage;

import java.io.File;
import java.util.List;

import co.edu.udea.software.arquitectura.docapp.R;
import co.edu.udea.software.arquitectura.docapp.models.DocumentModel;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.ViewHolder> {

    private Context context = null;
    private List<DocumentModel> documents;

    public DocumentAdapter(List<DocumentModel> documents) {
        this.documents = documents;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();

        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.card_upload_file, parent, false);
        ViewHolder holder = new ViewHolder(itemView);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final String title = documents.get(position).getTitle().substring(0, 11);
        final int preview = documents.get(position).getPreview();

        holder.title.setText(title);
        holder.preview.setImageResource(preview);

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, documents.get(position).getTitle(), Toast.LENGTH_SHORT).show();
            }
        });
        holder.card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(holder.card.getContext(), "Se descargara por navegador", Toast.LENGTH_SHORT).show();
                Uri uri = Uri.parse(documents.get(position).getUrl());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                holder.card.getContext().startActivity(intent);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return documents.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout card;
        TextView title;
        ImageView preview;

        public ViewHolder(View itemView) {
            super(itemView);

            card = itemView.findViewById(R.id.card_complete);
            title = itemView.findViewById(R.id.card_title);
            preview = itemView.findViewById(R.id.preview);
        }
    }
}
