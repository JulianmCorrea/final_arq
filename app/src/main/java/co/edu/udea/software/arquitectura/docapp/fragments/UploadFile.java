package co.edu.udea.software.arquitectura.docapp.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;

import co.edu.udea.software.arquitectura.docapp.R;

import static android.app.Activity.RESULT_OK;

public class UploadFile extends Fragment implements View.OnClickListener {
    final private static int DOCUMENT_SELECTED = 2;
    private Uri uri;
    private StorageReference mStorageRef;
    private ProgressDialog progressDialog;
    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private FirebaseUser user;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mRef = database.getReference("Documents");

    private Button subir, busqueda;
    private EditText nameF;

    public UploadFile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mStorageRef = FirebaseStorage.getInstance().getReference();
        View view = inflater.inflate(R.layout.fragment_upload_file, container, false);
        busqueda = view.findViewById(R.id.uploadbusqueda);
        busqueda.setOnClickListener(this);
        subir = view.findViewById(R.id.uploadsubir);
        nameF = view.findViewById(R.id.uploadname);
        subir.setOnClickListener(this);
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("subiendo....");
        progressDialog.setMessage("subiendo archivo por favor espere");
        progressDialog.setCancelable(false);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = auth.getCurrentUser();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.uploadbusqueda:

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                startActivityForResult(intent, DOCUMENT_SELECTED);
                break;
            case R.id.uploadsubir:

                progressDialog.show();
                final String name = getFileName(uri);

                //en Documents crear una carpeta para cada usuario y en uri ver como poner el nombre del archivo
                final StorageReference riversRef = mStorageRef.child("Documents").child(user.getUid()).child(name);
                UploadTask uploadTask = riversRef.putFile(uri);
                Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        // Continue with the task to get the download URL
                        return riversRef.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            HashMap<String, Object> result = new HashMap<>();
                            String subir = name.replace(".", "♫");
                            result.put(subir, task.getResult().toString());
                            mRef.child(user.getUid()).updateChildren(result);
                            uri = null;
                            Fragment fr = new MainFragment();
                            FragmentTransaction transaction = getFragmentManager().beginTransaction();
                            transaction.replace(R.id.fragmentContainer, fr);
                            transaction.addToBackStack(null);
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Succes", Toast.LENGTH_LONG).show();
                            transaction.commit();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == DOCUMENT_SELECTED) {
            if (resultCode == RESULT_OK) {

                uri = data.getData();
                subir.setEnabled(true);
                nameF.setText(getFileName(uri));
                Toast.makeText(getActivity(), getFileName(uri), Toast.LENGTH_LONG).show();


            }
        }
    }

    private String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContext().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


}
