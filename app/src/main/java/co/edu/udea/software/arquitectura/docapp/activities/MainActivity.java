package co.edu.udea.software.arquitectura.docapp.activities;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import co.edu.udea.software.arquitectura.docapp.fragments.MainFragment;
import co.edu.udea.software.arquitectura.docapp.R;
import co.edu.udea.software.arquitectura.docapp.fragments.PerfilUpdateFragment;
import co.edu.udea.software.arquitectura.docapp.fragments.UploadFile;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FragmentManager fragmentManager;
    private FirebaseAuth mAuth;
    private ImageView foto;
    private TextView name, email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);
        foto = header.findViewById(R.id.navFoto);
        name = header.findViewById(R.id.navUser);
        email = header.findViewById(R.id.navData);
        FirebaseUser user = mAuth.getCurrentUser();
        Glide.with(getApplicationContext())
                .load(user.getPhotoUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(foto);
        name.setText(user.getDisplayName());
        email.setText(user.getEmail());

        fragmentManager = getSupportFragmentManager();

        addFragment(new MainFragment(), true, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.main:
                addFragment(new MainFragment(), true, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                break;

            case R.id.logout:
                mAuth.signOut();
                this.finish();
                break;
            case R.id.profile:
                addFragment(new PerfilUpdateFragment(), true, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                break;
            case R.id.upload:
                addFragment(new UploadFile(), true, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                break;

            default:
                addFragment(new MainFragment(), true, FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void addFragment(Fragment fragment, boolean addBackStack, int animation) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        String backStack = fragment.getClass().getName();

        if (addBackStack)
            transaction.addToBackStack(backStack);

        transaction
                .setTransition(animation)
                .replace(R.id.fragmentContainer, fragment, backStack)
                .commit();
    }
}
